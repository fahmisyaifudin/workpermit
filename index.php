
<?php

$login = isset($_GET['login']) ? $_GET['login'] : false;

?>

<!DOCTYPE html>
<html>
<head>
	<title>Safe Work Permit</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/bootsnav.css">
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>




</head>
<body>
<!-- login area start -->
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100">
                <form action="proseslogin.php" method="post">
                    <div class="login-form-head">
                        <h4>Login</h4>
                    </div>
                    <div class="login-form-body">
                        <div class="form-gp">
                            Username
                            <input type="text" name="username">
                            <i class="ti-name"></i>
                        </div>
                        <div class="form-gp">
                            Password
                            <input type="password" id="exampleInputPassword1" name="pass">
                            <i class="ti-lock"></i>
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-6 text-right">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </div>
                        <?php
							                  if($login){
								                          echo '<div class="alert alert-danger">
    									                             <strong>ERROR : </strong> Username dan Password Salah
  									                                        </div>';
							                            }
						             ?>
                        <div class="submit-btn-area">
                            <button id="form_submit" name="submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                        </div>
                        <div class="form-footer text-center mt-5">
                            <p class="text-muted">Don't have an account? <a href="register.html">Sign up</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

</body>

</html>
