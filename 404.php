<!DOCTYPE html>
<html>
<head>
	<title>Safe Work Permit</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>




</head>
<body>
  <div class="error-area ptb--100 text-center">
          <div class="container">
              <div class="error-content">
                  <h2>404</h2>
                  <p>Ooops! Something went wrong .</p>
                  <a href="index.php">Back to Dashboard</a>
              </div>
          </div>
      </div>

</body>

</html>
