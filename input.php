<?php
session_start();
$username = isset($_SESSION['username']) ? $_SESSION['username'] : false;
$jabatan = isset($_SESSION['jabatan']) ? $_SESSION['jabatan'] : false;
	if(!$username){
		header("location: 404.php");
	}
	if ($jabatan =='BJP' || $jabatan =='UPIK3L' || $jabatan =='Bengkel') {
		header("location: main.php");
	}
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Input Data Permit</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
		<script src="assets/js/swipe.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
<body>
	<body data-spy="scroll" data-target=".navbar" data-offset="50">

	<nav class="navbar bg-dark navbar-dark navbar-expand-sm fixed-top">
			<a href="#" class="navbar-brand">SAFE WORK PERMIT</a>

			<button class="navbar-toggler" type="button" data-toggle = "collapse" data-target="#menukita">
				<span class="navbar-toggler-icon"> </span>

			</button>

			<div class="collapse navbar-collapse" id="menukita">
				<ul class="navbar-nav ml-auto">
					<li><a  class="nav-link" href="main.php">Home</a></li>
					<li><a class="nav-link" href="input.php">Input</a></li>
					<li class="nav-item dropdown">
      					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"><?php echo $username;?></a>
      				<div class="dropdown-menu">
        				<a class="dropdown-item" href="#">Profile</a>
        				<a class="dropdown-item" href="proseslogout.php">Logout</a>
      				</div>
    				</li>
				</ul>
			</div>

		</nav>
	<div class="container"> <!-- START OF CONTAINER -->
	<div class="row row-padding" id="inputkaryawan">
		<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
                <h4 class="header-title">Input Data Karyawan</h4>
					<p class="text-muted font-14 mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris turpis libero, laoreet nec augue et, dignissim elementum libero. fermentum</p>
						<form action="prosesdata.php" id="myForm" method="POST">
                            <div class="form-check">
  								<input class="form-check-input" type="radio" name="WorkPermitType" value="Cold Work" checked>
  								<label class="form-check-label" for="exampleRadios1">
    								Cold Work
  								</label>
							</div>
							<div class="form-check">
  								<input class="form-check-input" type="radio" name="WorkPermitType" value="Hot Work">
  								<label class="form-check-label" for="exampleRadios2">
    								Hot Work
  								</label>
							</div>
										<!-- Input Date -->
							<div class="form-group">
                                <label for="example-date-input" class="col-form-label">Date</label>
                                 <input class="form-control" type="date" name="Date" value="2018-03-05" id="example-date-input">
                            </div>

                             <div class="form-group">
                                    <label class="col-form-label">Bengkel</label>
										<select class="form-control" name="Bengkel" onchange="myFunction()">
											<option value="">(select type)</option>
                                            <option value="Perkakas" id="Perkakas">Perkakas</option>
                                            <option value="Konstruksi" id="Konstruksi">Konstruksi</option>
                                            <option value="CNC" id="CNC">CNC</option>
                                            <option value="NonMetal" id="NonMetal">NonMetal</option>
                                         </select>
                              </div>
                             <div class="form-group">
  								<label for="comment">Deskripsi dan Pekerjaan</label>
  								<textarea class="form-control" rows="5" name="DeskripsidanPekerjaan"></textarea>
							 </div>
							 

			</div>
		</div>
		</div>
	</div>
	<!-- END OFSWIPE 1-->
	<!-- SWIPE 2-->

	<div class="row pt-4" id="inputkaryawan">
		<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
					<h5>Pemohon Permit</h5>
							<small>Pemohon 1</small>
                             <div id="form-group">
                                <label for="example-text-input" class="col-form-label">Nama</label>
                                <input class="form-control" type="text" id="example-text-input" name="Nama[]">
                             </div>
                             <div class="form-group">
                                 <label for="example-search-input" class="col-form-label">NIP</label>
                                 <input class="form-control" type="search" id="example-search-input" name="Nip[]">
                              </div>
                              <small>Pemohon 2</small>
                              <div id="form-group">
                                <label for="example-text-input" class="col-form-label">Nama</label>
                                <input class="form-control" type="text" id="example-text-input" name="Nama[]">
                             </div>
                             <div class="form-group">
                                 <label for="example-search-input" class="col-form-label">NIP</label>
                                 <input class="form-control" type="search" id="example-search-input" name="Nip[]">
                              </div>
                              <small>Pemohon 3</small>
                              <div id="form-group">
                                <label for="example-text-input" class="col-form-label">Nama</label>
                                <input class="form-control" type="text" id="example-text-input" name="Nama[]">
                             </div>
                             <div class="form-group">
                                 <label for="example-search-input" class="col-form-label">NIP</label>
                                 <input class="form-control" type="search" id="example-search-input" name="Nip[]">
                              </div>
                              <small>Pemohon 4</small>
                              <div id="form-group">
                                <label for="example-text-input" class="col-form-label">Nama</label>
                                <input class="form-control" type="text" id="example-text-input" name="Nama[]">
                             </div>
                             <div class="form-group">
                                 <label for="example-search-input" class="col-form-label">NIP</label>
                                 <input class="form-control" type="search" id="example-search-input" name="Nip[]">
                              </div>
                        
			</div>
		</div>
		</div>
	</div>
	<!-- END OF SWIPE 2-->
	<!-- START of SWIPE3-->
	<div class="row pt-4" id="inputkaryawan"> <!-- STARTof FORM3-->
		<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
            <h5 class="margin-bottom">Peralatan Yang Dipakai</h5>
            	<div id="pilihanKonstruksi">
				<h6 class="margin-bottom-md"><b>Bengkel Konstruksi</b></h6>
					<div class="row">
						<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Hydrolic Iron Worker Machine">Hydrolic Iron Worker Machine</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Hydrolic Band Saw Machine">Hydrolic Band Saw Machine</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="3-Rol Machine">3-Rol Machine</label>
							</div>
                    	</div>
						<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Hacksaw Machine">Hacksaw Machine</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Drilling Machine">Drilling Machine</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Shearing Machine">Shearing Machine</label>
							</div>
                    	</div>
						<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Mesin Las OAW">Mesin Las OAW</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelKonstruksi[]" value="Mesin Las SMAW">Mesin Las SMAW</label>
							</div>
						</div>
          			</div>
          		</div>
          		<div id="pilihanPerkakas">	
            	<h6 class="margin-bottom-md"><b>Bengkel Perkakas</b></h6>
					<div class="row">
						<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox"  name="BengkelPerkakas[]" value="Mesin Bubut">Mesin Bubut</label>
							</div>
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelPerkakas[]" value="Mesin Bor">Mesin Bor</label>
							</div>
                  		</div>
                  		<div class="col-md-4">
                        	<div class="checkbox">
  								<label><input type="checkbox" name="BengkelPerkakas[]" value="Mesin Frais ">Mesin Frais </label>
							</div>
                  		</div>
                  		<div class="col-md-4">
                        	<div class="checkbox">
  								<label><input type="checkbox" name="BengkelPerkakas[]" value="Mesin Scrab">Mesin Scrab</label>
							</div>
						</div>
              	  	</div>
              	 </div>
              	<div id="pilihanCNC">
            	<h6 class="margin-bottom-md-up"><b>Bengkel CNC</b></h6>
              		<div class="row">
  						<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelCNC[]" value="Plasma Cutting">Plasma Cutting</label>
							</div>
						</div>
                    	<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelCNC[]" value="EDM">EDM</label>
							</div>
                    	</div>
                    	<div class="col-md-4">
							<div class="checkbox">
  								<label><input type="checkbox" name="BengkelCNC[]" value="Laser Cutting">Laser Cutting.</label>
							</div>
                    	</div>
					</div>
				</div>
				<div id="pilihanNonMetal">
				<h6 class="margin-bottom-md-up"><b>Bengkel Non Metal</b></h6>
		             <div class="row">
		  				<div class="col-md-4">
							<div class="checkbox">
		  						<label><input type="checkbox" name="BengkelNonMetal[]" value="Wood Turing Machine" >Wood Turing Machine</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="checkbox">
		  						<label><input type="checkbox" name="BengkelNonMetal[]" value="Sircular Saw and Turning">Sircular Saw and Turning</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="checkbox">
								<label><input type="checkbox" name="BengkelNonMetal[]" value="Bend Saw">Bend Saw</label>
							</div>
						</div>
					</div>
				</div>
					<button type="button submit" class="btn btn-primary float-right" style="margin-left: 5px; ">Finish</button>
		</form>

			</div> <!-- END of Card Bodu -->
		</div> <!-- END of Card -->
		</div> <!-- END of Col -->
	</div> <!-- END of FORM 3-->
</div> <!-- END OF CONTAINER -->

<footer>
  <div class="pt-4 footer-area text-center">
      <p> © Copyright 2019. All right reserved. Template by <a href="#">Anonymous</a>.</p>
  </div>
</footer>
<script type="text/javascript">

function myFunction() {
	if(document.getElementById("Perkakas").selected){
		document.getElementById("pilihanCNC").remove();
		document.getElementById("pilihanKonstruksi").remove();
		document.getElementById("pilihanUjiBahan").remove();
		document.getElementById("pilihanNonMetal").remove();
	}else if(document.getElementById("CNC").selected){
		document.getElementById("pilihanPerkakas").remove();
		document.getElementById("pilihanKonstruksi").remove();
		document.getElementById("pilihanUjiBahan").remove();
		document.getElementById("pilihanNonMetal").remove();
	}else if(document.getElementById("Konstruksi").selected){
		document.getElementById("pilihanCNC").remove();
		document.getElementById("pilihanPerkakas").remove();
		document.getElementById("pilihanUjiBahan").remove();
		document.getElementById("pilihanNonMetal").remove();
	}else if(document.getElementById("UjiBahan").selected){
		document.getElementById("pilihanCNC").remove();
		document.getElementById("pilihanKonstruksi").remove();
		document.getElementById("pilihanPerkakas").remove();
		document.getElementById("pilihanNonMetal").remove();
	}else if(document.getElementById("NonMetal").selected){
		document.getElementById("pilihanCNC").remove();
		document.getElementById("pilihanKonstruksi").remove();
		document.getElementById("pilihanUjiBahan").remove();
		document.getElementById("pilihanPerkakas").remove();
	}
	
}
</script>


</body>

</div>
</div>
</html>
