<?php
include_once("function/koneksi.php");
session_start();
$username = isset($_SESSION['username']) ? $_SESSION['username'] : false;
$jabatan = isset($_SESSION['jabatan']) ? $_SESSION['jabatan'] : false;
if(!$username){
		header("location: 404.php");
	}
if ($jabatan=='Perkakas' || $jabatan=='CNC' || $jabatan=='Konstruksi' || $jabatan=='UjiBahan' || $jabatan=='NonMetal') {
   $query = mysqli_query($koneksi, "SELECT * FROM tabel1 WHERE Lokasi='$jabatan'" );
 }else{
   $query = mysqli_query($koneksi, "SELECT * FROM tabel1" );
 } 
$query_1 = mysqli_query($koneksi, "SELECT * FROM bengkel" );
$waktunow = time();

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Safe Work Permit</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">

	<script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>


</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar bg-dark navbar-dark navbar-expand-sm fixed-top">
			<a href="#" class="navbar-brand">SAFE WORK PERMIT</a>

			<button class="navbar-toggler" type="button" data-toggle = "collapse" data-target="#menukita">
				<span class="navbar-toggler-icon"> </span>

			</button>

			<div class="collapse navbar-collapse" id="menukita">
				<ul class="navbar-nav ml-auto">
					<li><a  class="nav-link" href="main.php">Home</a></li>
					<li><a class="nav-link" href="input.php">Input</a></li>
					<li class="nav-item dropdown">
      					<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"><?php echo $username;?></a>
      				<div class="dropdown-menu">
                <a class="dropdown-item" href="#">Profile</a>
                <?php
                if($jabatan=='Admin'){
                    echo '<a class="dropdown-item" href="http://localhost/admin_wp">Admin Panel</a>';
                }
                ?>
                <a class="dropdown-item" href="proseslogout.php">Logout</a>
      				</div>
    				</li>
				</ul>
			</div>

		</nav>




<div class="container">
	<div class="row row-padding" id="datakaryawan">
		<div class="col-sm-12">
		<div class="card">
			<div class="card-body">
      <div class="table-responsive">
			<table class="display" id="example">
				<h4 class="header-title">Data Table Karyawan</h4>
				<p class="text-muted font-14 mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris turpis libero, laoreet nec augue et, dignissim elementum libero. fermentum</p>
  				<thead class="thead-dark">
    				<tr>
      					<th scope="col" class="column-primary">No Permit</th>
      					<th scope="col">Tanggal</th>
      					<th scope="col">Lokasi</th>
      					<th scope="col">Deskripsi</th>
      					<th scope="col" >Action</th>
								<th scope="col" >Disetujui Oleh Ketua</th>
                <th scope="col" class="column-primary">Status</th>

    				</tr>
  				</thead>
  			<tbody>

      					<?php

                while ($row = mysqli_fetch_assoc($query)) {
                	$statusID = $row['ID'];
                	$status = mysqli_query($koneksi, "SELECT * FROM Status WHERE ID='$statusID'" );
                	$rowstatus = mysqli_fetch_assoc($status);
                  echo '<tr>	<th scope="row">'.$row['ID'].'</th>
          						<td>'.$row['Tanggal'].'</td>
          						<td>'.$row['Lokasi'].'</td>
          						<td>'.$row['Deskripsi'].'</td>
                      			<td><a href=delete.php?id='.$row['ID'].'><button type="button" class="btn btn-primary"><i class="far fa-trash-alt"></i></button><span style="display:inline-block; width: 5px;"></a></span><a href=print.php?id='.$row['ID'].'><button type="button" class="btn btn-primary"><i class="fas fa-print"></i></button></a> 
                      			</td>';
                      			if ($rowstatus['ACCKepalaBengkel']) {
                      				echo '<td><a href="prosesacc.php?acc=Bengkel&id='.$row['ID'].'"><button type="button" class="btn btn-success">Bengkel</button></a><span style="display:inline-block; width: 5px;"></span>';
                      			}else{
                      				echo '<td><a href="prosesacc.php?acc=Bengkel&id='.$row['ID'].'"><button type="button" class="btn btn-danger">Bengkel</button></a><span style="display:inline-block; width: 5px;"></span>';
                      			}
                      			if ($rowstatus['ACCKetuaBJP']) {
                      				echo '<a href="prosesacc.php?acc=BJP&id='.$row['ID'].'"><button type="button" class="btn btn-success">BJP</button></a><span style="display:inline-block; width: 5px;"></span>';
                      			}else{
                      				echo '<a href="prosesacc.php?acc=BJP&id='.$row['ID'].'"><button type="button" class="btn btn-danger">BJP</button></a><span style="display:inline-block; width: 5px;"></span>';
                      			}
                      			if ($rowstatus['ACCKetuaUPIK3L']) {
                      				echo '<a href="prosesacc.php?acc=UPIK3L&id='.$row['ID'].'"><button type="button" class="btn btn-success">UPIK3L</button></td></a>';
                      			}else{
                      				echo '<a href="prosesacc.php?acc=UPIK3L&id='.$row['ID'].'"><button type="button" class="btn btn-danger">UPIK3L</button></td></a>';
                      			}
                    $valid = $rowstatus['ACCKepalaBengkel']*$rowstatus['ACCKetuaBJP']*$rowstatus['ACCKetuaUPIK3L'];
                    $timestamp = strtotime($rowstatus['Waktu']) + 60*60*8;
                    //var_dump($timestamp);
                    //var_dump($waktunow);
                    //die;
                    if ($valid==1) {
                      if ($waktunow > $timestamp) {
                        echo '<td><button type="button" class="btn btn-info">Expired</button></td></tr>';
                      }else{
                        echo '<td><button type="button" class="btn btn-success">Valid</button></td></tr>';
                      }
                    }else{
                       echo '<td><button type="button" class="btn btn-danger">Not Valid</button></td></tr>';
                    }
                  }


                ?>


  			</tbody>
		</table>
		</div>
    </div>
		</div>
		</div>

	</div>


	</div>


</div>

<script>
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>




</body>

</html>
