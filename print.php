<?php
session_start();
$username = isset($_SESSION['username']) ? $_SESSION['username'] : false;
	if(!$username){
		header("location: 404.php");
	}
  ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : false;
include_once("function/koneksi.php");
$query = mysqli_query($koneksi, "SELECT * FROM tabel1 WHERE ID='$id' " );
while ($row = mysqli_fetch_assoc($query)) {
	$WorkPermitType = $row['Worktype'];
  $Date = $row['Tanggal'];
  $Lokasi = $row['Lokasi'];
  $DeskripsidanPekerjaan = $row['Deskripsi'];
}
$nameofday = date('D', strtotime($Date));
$nameofday = ConvertToIndo($nameofday);
$query = mysqli_query($koneksi, "SELECT * FROM tabel2 WHERE ID='$id' " );
while ($row = mysqli_fetch_assoc($query)) {
	$Nama = $row['Nama'];
  $Nip = $row['Nip'];
  $Bengkel = $row['Bengkel'];
}
$str_arr = explode (",", $Nama);
$Nama = $str_arr;
$str_arr = explode (",", $Nip);
$Nip = $str_arr;

$query = mysqli_query($koneksi, "SELECT * FROM Status WHERE ID='$id' " );
while ($row = mysqli_fetch_assoc($query)) {
  if(isset($row['NamaBengkel'])){
      $namaBengkel = $row['NamaBengkel'];
  }else{
      $namaBengkel='';
  }
  $ttdBengkel = $row['ttdBengkel'];
  $namaBJP = $row['NamaBJP'];
  $ttdBJP = $row['ttdBJP'];
  $namaUPIK3L = $row['NamaUPIK3L'];
  $ttdUPIK3L = $row['ttdUPIK3L'];
}
$query = mysqli_query($koneksi, "SELECT * FROM Bengkel WHERE ID='$id' " );
while ($row = mysqli_fetch_assoc($query)) {
	$PeralatanYangDipakai = $row['PeralatanYangDipakai'];
}

$str_arr = explode (",", $PeralatanYangDipakai);
$PeralatanYangDipakai = $str_arr;
$n = count($PeralatanYangDipakai);
$dataEquipment = [];
$dataHazard = [];
$dataRiskControl = [];

for ($i=1; $i <count($PeralatanYangDipakai) ; $i++) { 
  $query = mysqli_query($koneksi, "SELECT * FROM Peralatan WHERE PeralatanYangDipakai='$PeralatanYangDipakai[$i]' " );
  $result[$i] = mysqli_fetch_assoc($query);
  $str_arr = explode (",", $result[$i]['Hazard']);
  $Hazard[$i] = $str_arr;
  $str_arr = explode (",", $result[$i]['RiskControl']);
  $RiskControl[$i] = $str_arr;
  $str_arr = explode (",", $result[$i]['Equipment']);
  $Equipment[$i] = $str_arr;
  $dataEquipment = array_unique(array_merge($Equipment[$i], $dataEquipment));
  $dataHazard = array_unique(array_merge($Hazard[$i], $dataHazard));
  $dataRiskControl = array_unique(array_merge($RiskControl[$i], $dataRiskControl));  
}

 ?>
 <html>
 <head>
 	<title>Print Work Permit</title>
 	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
 	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 	<link rel="stylesheet" href="assets/css/style.css">
  <style type="text/css">
    
    body{
    font-family: "Times New Roman", Times, serif;
    color: black;
    letter-spacing: 0.5px;
    background: white;
    }
  </style>
 	<script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/printThis.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/bootstrap.min.js"></script>

 </head>
 <body style="background-color: white;">


 <div class="container">
	 <div id="printarea">
 <div class="row">
  <div class="col-sm-4">
    No. Permit <br>
    <h3><?php echo $id; ?></h3>
  </div>
  <div class="col-sm-8">
    <h3>SAFE WORK PERMIT</h3>
  </div>
 </div>
  <div class="row">
    <?php echo "Jenis : $WorkPermitType" ?><br>
    <?php echo "Hari/Tanggal : $nameofday $Date" ?><br>
    <?php echo "Lokasi  : Bengkel $Bengkel" ?><br>
    <?php echo "Deskripsi dan Pekerjaan : $DeskripsidanPekerjaan" ?><br>
  </div>
	<br><h5 class="text-center">Pemohon Permit</h5>
  <div class="row">
    <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIP</th>
        <th>Tanda Tangan</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php echo '<td>1</td>
        <td>'.$Nama[1].'</td>
        <td>'.$Nip[1].'</td>
        <td></td>
        <td></td>'; ?>
      </tr>
      
    </tbody>
  </table>
  </div>
	<br><h5 class="text-center">Peralatan yang dipakai</h5>
  <div class="row">
    <div class="col-sm-3">
        <?php
          for ($i=1; $i <count($PeralatanYangDipakai) ; $i++) { 
              echo '<input type="checkbox" disabled="disabled" checked="checked">';
              echo "$PeralatanYangDipakai[$i] <br>";
          }
        ?>
    </div>

  </div>
  <div class="row">
    <div class="col-sm-4">
      <br><h5 class="text-center">Hazard</h5>
        <?php
              foreach ($dataHazard as $value) {
                echo '<input type="checkbox" disabled="disabled" checked="checked">';
                echo "$value <br>";
              }          
        ?>
    </div>
    <div class="col-sm-4">
      <br><h5 class="text-center">Risk Control</h5>
        <?php
              foreach ($dataRiskControl as $value) {
                echo '<input type="checkbox" disabled="disabled" checked="checked">';
                echo "$value <br>";              }          
        ?>

    </div>  
    <div class="col-sm-4">
      <br><h5 class="text-center">Equipment</h5>
      <?php
              foreach ($dataEquipment as $value) {
                echo '<input type="checkbox" disabled="disabled" checked="checked">';
                echo "$value <br>";
              }         
                
        ?>

			
    </div>
  </div>

		<div class="row row-padding row-padding-bottom">
			<div class="col-sm-12 text-center">
				<h5><b>Persetujuan Work Permit</b></h5>
		</div>
	</div>
		<div class="row row-padding-bottom">
			<div class="col-md-6 text-center">
				<h5>Kepala Bengkel</h5>
				<br>
        <?php if(isset($namaBengkel)==false || $namaBengkel=='') echo '<br><br><br><br><br><br>'; else echo '<img src="http://localhost/admin_wp/assets/img/'.$ttdBengkel.'" style="width: 300px; height: 150px"><br>'; ?>
				(<?php if(isset($namaBengkel)) echo "$namaBengkel"; else echo "............"; ?>)
			</div>
			<div class="col-md-6 text-center">
				<h5>Kepala BJP</h5>
				<br>
          <?php if(isset($namaBJP)==false || $namaBJP=='') echo '<br><br><br><br><br><br>'; else echo '<img src="http://localhost/admin_wp/assets/img/'.$ttdBJP.'" style="width: 300px; height: 150px"><br>'; ?>
				(<?php if(isset($namaBJP)) echo "$namaBJP"; else echo "............"; ?>)
			</div>
		</div>
		<div class="row row-padding-bottom">
			<div class="col-sm-12 text-center">
				<h5>Kepala UPIK3L</h5>
				  <br>
         <?php if(isset($namaUPIK3L)==false || $namaUPIK3L=='') echo '<br><br><br><br><br><br>'; else echo '<img src="http://localhost/admin_wp/assets/img/'.$ttdUPIK3L.'" style="width: 300px; height: 150px"><br>'; ?>
        (<?php if(isset($namaUPIK3L)) echo "$namaUPIK3L"; else echo "............"; ?>)
			</div>
		</div>
	<button id="basic" class="btn btn-primary float-right" style="margin-right: 5px; ">Print</button>


</div>
</div>
<script>
$('#basic').on("click", function () {
      $('#printarea').printThis({
				importStyle: false,
				loadCSS: "assets/css/bootstrap.min.css",

      });
    });
</script>
<?php

function ConvertToIndo($a){
  if($a=='Mon')
      return 'Senin';
  elseif ($a=='Sun')
      return 'Minggu';
  elseif ($a=='Tue')
      return 'Selasa';
  elseif ($a=='Wed')
      return 'Rabu';
  elseif ($a=='Thu')
      return 'Kamis';
  elseif ($a=='Fri')
      return 'Jumat';
  elseif ($a=='Sat')
      return 'Sabtu';
  else
      return $a;
}

?>

 </body>

 </html>
